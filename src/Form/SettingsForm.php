<?php

namespace Drupal\pdf_metadata\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure PDF Metadata settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pdf_metadata.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pdf_metadata_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pdf_metadata.settings');

    $form['extra_gs_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Extra Ghostscript Options'),
      '#description' => $this->t('Enter Additional Ghostscript command arguments, one per line.'),
      '#default_value' => implode("\n", $config->get('extra_gs_options')),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $extra_gs_options_config = $form_state->getValue('extra_gs_options');
    $extra_gs_options = array_filter(array_map('trim', explode("\n", $extra_gs_options_config)));

    $this->configFactory->getEditable('pdf_metadata.settings')
      ->set('extra_gs_options', $extra_gs_options)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
