<?php

namespace Drupal\pdf_metadata\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\token\TokenEntityMapperInterface;

/**
 * Provides a service with helper functions useful during metadata creation.
 */
class MetadataCreationService {
  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Token entity type mapper service.
   *
   * @var \Drupal\token\TokenEntityMapperInterface
   */
  protected $tokenEntityMapper;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Drupal config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The file_system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\token\TokenEntityMapperInterface $token_entity_mapper
   *   The token entity type mapper service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\Utility\Token $token
   *   The token.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TokenEntityMapperInterface $token_entity_mapper, ConfigFactoryInterface $config_factory, FileSystemInterface $file_system, Token $token, LoggerChannelFactoryInterface $logger_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->tokenEntityMapper = $token_entity_mapper;
    $this->configFactory = $config_factory;
    $this->fileSystem = $file_system;
    $this->token = $token;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * Check for replacement tokens.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to provide token replacements for.
   * @param array $settings
   *   The settings for the given entity.
   *
   * @return array
   *   An array of the replaced tokens for each meta type.
   */
  public function checkReplacementTokens(EntityInterface $entity, array $settings): array {
    $metadata = [];
    $token_data = [$entity->getEntityTypeId() => $entity];
    foreach (array_keys($settings['meta_types']) as $type) {
      $metadata[$type] = $this->token->replace($settings['meta_types'][$type], $token_data, ['clear' => TRUE]);
    }
    return $metadata;
  }

  /**
   * Check if the given reference field has PDF Metadata Reference Enabled.
   *
   * @param array $references
   *   References to validate if PDF Metadata Reference is enabled.
   *
   * @return array
   *   Returns file fields with PDF Metadata References enabled.
   */
  public function validateReferenceFields(array $references): array {
    $file_fields = [];
    foreach ($references as $reference) {
      foreach ($reference['field']->referencedEntities() as $ref_entity) {
        if (!$ref_entity instanceof ContentEntityInterface) {
          continue;
        }

        foreach ($ref_entity->getFields() as $ref_field) {
          $settings = $this->getThirdPartySettings($ref_field);

          // Only replace references with 'reference_enabled' set to TRUE.
          if ($ref_field instanceof FileFieldItemList && $settings && $settings['reference_enabled']) {

            // We need to pass the reference entity field to be checked.
            $reference['entity'] = $ref_entity;
            $reference['field'] = $ref_field;
            $file_fields[] = $reference;
          }
        }
      }
    }
    return $file_fields;
  }

  /**
   * Check if the given file field has PDF Metadata enabled.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity that was updated and should be checked for PDF Metadata.
   *
   * @return array
   *   Array of file fields with PDF Metadata enabled separated by type.
   */
  public function validateFields(EntityInterface $entity): array {
    $fields = ['file_fields' => [], 'reference_fields' => []];
    if ($entity instanceof ContentEntityInterface) {
      foreach ($entity->getFields() as $field) {
        $settings = $this->getThirdPartySettings($field);

        // Only add items with 'enabled' set to TRUE.
        if (!$settings || !$settings['enabled']) {
          continue;
        }

        if ($field instanceof EntityReferenceFieldItemList) {
          $fields['reference_fields'][] = [
            'field' => $field,
            'settings' => $settings,
            'parent_entity' => $entity,
          ];
        }

        if ($field instanceof FileFieldItemList) {
          $fields['file_fields'][] = [
            'entity' => $entity,
            'field' => $field,
            'settings' => $settings,
          ];
        }
      }
    }
    return $fields;
  }

  /**
   * Get files, check mime types and get token values.
   *
   * @param array $file_fields
   *   Combined file fields for validating.
   *
   * @return array
   *   Combined files with metadata.
   */
  public function validateFiles(array $file_fields): array {
    $files = [];
    foreach ($file_fields as $item) {
      foreach ($item['field']->referencedEntities() as $file) {

        // Only replace if the file is a PDF, otherwise continue loop.
        if ($file->getMimeType() !== 'application/pdf') {
          continue;
        }

        $files[] = [
          'file' => $file,
          'metadata' => $this->checkReplacementTokens($item['parent_entity'] ?? $item['entity'], $item['settings']),
        ];
      }
    }
    return $files;
  }

  /**
   * Generate PDF for the given files with metadata.
   *
   * @param array $files
   *   Array of files with metadata.
   */
  public function generatePdfWithMetadata(array $files): void {
    foreach ($files as $file) {
      if (!file_exists($file['file']->getFileUri())) {
        $this->loggerFactory->get('pdf_metadata')->error('Error reading file.', [
          '@file' => $file['file']->getFileUri(),
        ]);
        continue;
      }

      // Catch all block in case of issues saving / converting.
      try {
        $filePath = $this->fileSystem->realpath($file['file']->getFileUri());
        $tempFilePath = $this->fileSystem->getTempDirectory() . '/' . basename($filePath);

        $config = $this->configFactory->get('pdf_metadata.settings');
        $extraOptions = $config->get('extra_gs_options') ?? [];

        $gsOptions = [
          '-dBATCH',
          '-dNOPAUSE',
          '-dSAFER',
          '-sDEVICE=pdfwrite',
          '-dCompatibilityLevel=1.4',
          '-dPrinted=false',
          '-dPDFSETTINGS=/prepress',
        ];

        // Combine with extra options, remove any options that are duplicated.
        if (!empty($extraOptions)) {
          $extraOptionKeys = array_map(fn($option) => explode('=', $option)[0], $extraOptions);
          $gsOptions = array_filter($gsOptions, fn($option) => !in_array(explode('=', $option)[0], $extraOptionKeys));
          $gsOptions = array_merge($gsOptions, $extraOptions);
        }

        $gsOptions = implode(' ', $gsOptions);

        // Construct the Ghostscript command to append metadata.
        $command = 'gs -o ' . escapeshellarg($tempFilePath) . ' ' . $gsOptions;
        $command .= ' -f ' . escapeshellarg($filePath) . ' -c "[';
        $command .= ' /Title (' . $this->sanitizePdfMetadata($file['metadata']['title']) . ')';
        $command .= ' /Author (' . $this->sanitizePdfMetadata($file['metadata']['author']) . ')';
        $command .= ' /Subject (' . $this->sanitizePdfMetadata($file['metadata']['subject']) . ')';
        $command .= ' /Keywords (' . $this->sanitizePdfMetadata($file['metadata']['keywords']) . ')';
        $command .= ' /DOCINFO pdfmark" 2>&1';

        // Execute the Ghostscript command.
        exec($command, $output, $returnCode);

        if ($returnCode === 0) {
          rename($tempFilePath, $filePath);
        }
        else {
          $errorOutput = implode("\n", $output);
          $this->loggerFactory->get('pdf_metadata')->error('Error executing GhostScript to write metadata to PDF. Return code: @returnCode. Output: @output', [
            '@returnCode' => $returnCode,
            '@output' => $errorOutput,
          ]);
        }

      }
      catch (\Exception $e) {
        $this->loggerFactory->get('pdf_metadata')->error('Error setting / writing metadata to PDF. See @error.', [
          '@error' => $e->getMessage(),
        ]);
      }

    }
  }

  /**
   * This function will perform all of the functions needed to append metadata.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that triggered the event.
   */
  public function appendMetadata(EntityInterface $entity): void {
    $fields = $this->validateFields($entity);
    $file_fields = $this->validateReferenceFields($fields['reference_fields']);
    $files = $this->validateFiles(array_merge($fields['file_fields'], $file_fields));

    // Generate PDFs with all the metadata.
    $this->generatePdfWithMetadata($files);
  }

  /**
   * Check if the field contains third party settings and return them.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field to check for third party settings.
   *
   * @return array
   *   Returns pdf_metadata from third party settings.
   */
  public function getThirdPartySettings(FieldItemListInterface $field): array {
    $definition = $field->getFieldDefinition();
    if (!isset($definition) || !($definition instanceof ThirdPartySettingsInterface)) {
      return [];
    }
    return $definition->getThirdPartySettings('pdf_metadata');
  }

  /**
   * Remove unsafe special characters from the metadata.
   *
   * @param string $metadata
   *   The value to check for third party settings.
   *
   * @return string
   *   Returns sanitized metadata value.
   */
  public function sanitizePdfMetadata(string $metadata): string {
    $pattern = '/[^A-Za-z0-9\s.,\-_:\?!@#\$%&\*\+=~]/';
    $sanitizedMetadata = preg_replace($pattern, '', $metadata);
    return $sanitizedMetadata;
  }

  /**
   * Utility function to add the metadata form fields to the form.
   *
   * @param array $form
   *   Form that we are currently configuring.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state that we are currently configuring.
   */
  public function addMetaFormFields(array &$form, FormStateInterface $form_state): void {
    /** @var \Drupal\field\Entity\FieldConfig $field */
    $field = $form_state->getFormObject()->getEntity();
    $definition = $field->getItemDefinition();
    $class = $field->getClass();
    ;

    if (!class_exists($class)) {
      return;
    }

    // Check if $fieldItem is an instance of any of the required classes.
    if (!(($fieldItem = new $class($definition)) instanceof FileFieldItemList ||
      $fieldItem instanceof EntityReferenceFieldItemList)) {
      return;
    }

    $settings = $field->getThirdPartySettings('pdf_metadata');

    $entity_info = $this->entityTypeManager->getDefinition($field->getTargetEntityTypeId());
    $type = $this->tokenEntityMapper->getTokenTypeForEntityType($entity_info->id());

    $form['settings']['pdf_metadata'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#weight' => 2,
      '#parents' => ['third_party_settings', 'pdf_metadata'],
    ];

    // Only show this option for entities that can be referenced.
    $form['settings']['pdf_metadata']['reference_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable PDF Metadata for Referencing Entities?'),
      '#default_value' => $settings['reference_enabled'] ?? FALSE,
      '#description' => $this->t('This allows other entity reference fields to write metadata to this field.'),
      '#states' => [
        'disabled' => [
          ':input[name="third_party_settings[pdf_metadata][enabled]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="third_party_settings[pdf_metadata][type]"]' => ['value' => 'media'],
        ],
      ],
    ];

    $form['settings']['pdf_metadata']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable PDF Metadata?'),
      '#default_value' => $settings['enabled'] ?? FALSE,
      '#description' => $this->t('PDF Metadata allows for automatic metadata appending.'),
      '#states' => [
        'disabled' => [
          ':input[name="third_party_settings[pdf_metadata][reference_enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['settings']['pdf_metadata']['details'] = [
      '#type' => 'details',
      '#title' => $this->t('PDF Metadata Settings'),
      '#weight' => 3,
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="third_party_settings[pdf_metadata][enabled]"]' => ['checked' => TRUE],
        ],
      ],
      '#parents' => ['third_party_settings', 'pdf_metadata'],
    ];

    $token_default = $type == 'node' ? '[' . $type . ':title]' : '[' . $type . ':name:value]';

    $form['settings']['pdf_metadata']['details']['meta_types']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 512,
      '#size' => 128,
      '#default_value' => $settings['meta_types']['title'] ?? $token_default,
    ];

    $form['settings']['pdf_metadata']['details']['meta_types']['author'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Author'),
      '#maxlength' => 512,
      '#size' => 128,
      '#default_value' => $settings['meta_types']['author'] ?? '',
    ];

    $form['settings']['pdf_metadata']['details']['meta_types']['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#maxlength' => 512,
      '#size' => 128,
      '#default_value' => $settings['meta_types']['subject'] ?? '',
    ];

    $form['settings']['pdf_metadata']['details']['meta_types']['keywords'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Keywords'),
      '#maxlength' => 512,
      '#size' => 128,
      '#default_value' => $settings['meta_types']['keywords'] ?? $token_default,
    ];

    $form['settings']['pdf_metadata']['details']['type'] = [
      '#type' => 'hidden',
      '#default_value' => $settings['type'] ?? $type,
    ];

    $form['settings']['pdf_metadata']['details']['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [''],
      '#weight' => 10,
    ];
    $form['settings']['pdf_metadata']['details']['token_tree']['#token_types'][] = $settings['type'] ?? $type;
  }

}
